<?php

/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 14/09/16
 * Time: 11:03
 */
class Exemplar
{
    private $id;
    private $livroId;
    private $bibliotecaId;
    private $edicao;
    private $anoPublicacao;
    private $numeroPaginas;

    public function __construct($data){

        //$this->id = $data["id"];
        $this->livroId = $data["livros_id"];
        $this->bibliotecaId = $data["biblioteca_id"];
        $this->edicao = $data["edicao"];
        $this->anoPublicacao = $data["ano_publicacao"];
        $this->numeroPaginas = $data["num_paginas"];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLivroId()
    {
        return $this->livroId;
    }

    /**
     * @param mixed $livroId
     */
    public function setLivroId($livroId)
    {
        $this->livroId = $livroId;
    }

    /**
     * @return mixed
     */
    public function getBibliotecaId()
    {
        return $this->bibliotecaId;
    }

    /**
     * @param mixed $bibliotecaId
     */
    public function setBibliotecaId($bibliotecaId)
    {
        $this->bibliotecaId = $bibliotecaId;
    }

    /**
     * @return mixed
     */
    public function getEdicao()
    {
        return $this->edicao;
    }

    /**
     * @param mixed $edicao
     */
    public function setEdicao($edicao)
    {
        $this->edicao = $edicao;
    }

    /**
     * @return mixed
     */
    public function getAnoPublicacao()
    {
        return $this->anoPublicacao;
    }

    /**
     * @param mixed $anoPublicacao
     */
    public function setAnoPublicacao($anoPublicacao)
    {
        $this->anoPublicacao = $anoPublicacao;
    }

    /**
     * @return mixed
     */
    public function getNumeroPaginas()
    {
        return $this->numeroPaginas;
    }

    /**
     * @param mixed $numeroPaginas
     */
    public function setNumeroPaginas($numeroPaginas)
    {
        $this->numeroPaginas = $numeroPaginas;
    }


}