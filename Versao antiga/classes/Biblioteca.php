<?php

/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 14/09/16
 * Time: 12:05
 */
class Biblioteca
{
    private $id;
    private $nome;
    private $endereco;

    public function __construct($data){

        $this->id = $data["id"];
        $this->endereco = $data["endereco"];
        $this->nome = $data["nome"];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param mixed $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }


}