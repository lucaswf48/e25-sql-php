<?php

/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 14/09/16
 * Time: 13:37
 */
class EmprestimoDAO implements DefaultDAO
{
    public function conecta()
    {

        $daw = new ConnectionFactory();
        return $daw->getConnection();
    }

    public function insert($object)
    {
        $conexao = $this->conecta();

        echo $object["livros_id"];

        $emprestimo = new Emprestimo($object);

        $dados = "(\"".$emprestimo->getExemplaresid()."\",\"".$emprestimo->getUsuariosId()."\",\"".$emprestimo->getBibliotecasId()."\",\"".$emprestimo->getDataEmprestimo()."\",\"".$emprestimo->getDataDevolucao()."\");";

        if(!mysqli_query($conexao,"INSERT INTO  emprestimos VALUES".$dados)){

            echo("Error description: " . mysqli_error($conexao)."<br/>");
            return false;
        }

        mysqli_close($conexao);

        return true;
    }

    public function delete($object)
    {
        // TODO: Implement delete() method.
    }

    public function deleteAll()
    {
        // TODO: Implement deleteAll() method.
    }

    public function update()
    {
        // TODO: Implement update() method.
    }

    public function getById($id)
    {
        // TODO: Implement getById() method.
    }

    public function getNumberEmprestimosPorUsuario(){

        $conexao = $this->conecta();

        $dado = "SELECT usuarios.nome,count(emprestimos.usuarios_id) AS emprestimos FROM emprestimos JOIN usuarios ON usuarios.id = emprestimos.usuarios_id GROUP BY usuarios_id ORDER BY count(emprestimos.usuarios_id);";

        $resultado = mysqli_query($conexao,$dado);
        $linhas = mysqli_num_rows($resultado);

        for($i = 0; $i < $linhas; $i++){

            $emprestimo = mysqli_fetch_array($resultado);
            foreach($emprestimo as $key => $value){
                echo "$key: $value<br/>";
            }
        }
        mysqli_close($conexao);
    }

    public function getNumberEmprestimosPorBiblioteca(){

        $conexao = $this->conecta();

        $dado = "SELECT bibliotecas.nome,count(emprestimos.exemplares_id) AS emprestimos FROM emprestimos JOIN bibliotecas ON bibliotecas.id = emprestimos.bibliotecas_id GROUP BY bibliotecas_id ORDER BY count(emprestimos.exemplares_id);";

        $resultado = mysqli_query($conexao,$dado);
        $linhas = mysqli_num_rows($resultado);

        for($i = 0; $i < $linhas; $i++){

            $emprestimo = mysqli_fetch_array($resultado);
           // var_dump($emprestimo);
            foreach($emprestimo as $key => $value){
                echo "$key: $value<br/>";
            }
        }

        mysqli_close($conexao);
    }

    public function getByUsuarioNome($object){

        $conexao = $this->conecta();

        $nomeUsuario = $object["nome"];


        $dado = "SELECT * FROM emprestimos JOIN usuarios ON usuarios.id = emprestimos.usuarios_id WHERE usuarios.nome = '$nomeUsuario'";
        $resultado = mysqli_query($conexao,$dado);
        $linhas = mysqli_num_rows($resultado);

        for($i = 0; $i < $linhas; $i++){
            $emprestimo = mysqli_fetch_array($resultado);

            foreach($emprestimo as $key => $value){
                echo "$key: $value<br/>";
            }
        }
        mysqli_close($conexao);
    }

    public function getByBiblioteca($object){

        $conexao = $this->conecta();

        $nomeBiblioteca = $object["nome"];


        $dado = "SELECT * FROM emprestimos JOIN bibliotecas ON bibliotecas.id = emprestimos.bibliotecas_id WHERE bibliotecas.nome = '$nomeBiblioteca'";
        $resultado = mysqli_query($conexao,$dado);
        $linhas = mysqli_num_rows($resultado);

        for($i = 0; $i < $linhas; $i++){
            $emprestimo = mysqli_fetch_array($resultado);

            foreach($emprestimo as $key => $value){
                echo "$key: $value<br/>";
            }
        }

        mysqli_close($conexao);
    }

    public function getByName($name){

        $conexao = $this->conecta();

        $dado = "SELECT * FROM livros WHERE nome LIKE '%".$name["nome"]."%'";

        $resultado = mysqli_query($conexao,$dado);
        $linhas = mysqli_num_rows($resultado);

        if($linhas == 0){
            return "Nenhum livro encontrado com esse nome";
        }


        for($i = 0; $i < $linhas; $i++){
            $livro = mysqli_fetch_array($resultado);
            echo "Nome: ".$livro["id"]." Id: ".$livro["ISBN"]." Autor: ".$livro["autor"]." Tema: ".$livro["tema"]." Editora: ".$livro["editora"]."<br/><br/>";
        }

        $id = $livro["id"];

        $exemplar = "SELECT * FROM exemplares WHERE livros_id = $id;";

        $resultado = mysqli_query($conexao,$exemplar);
        $linhas = mysqli_num_rows($resultado);

        if($linhas == 0){
            echo "SELECT * FROM exemplares WHERE livros_id = $id;";
            return "Nenhum exemplar econtrado.";
        }

        for($i = 0; $i < $linhas; $i++) {

            $exemplar = mysqli_fetch_array($resultado);

            $data["exemplares_id"] = $exemplar["id"];
            $data["usuarios_id"] = $name["id"];
            $data["bibliotecas_id"] = $exemplar["bibliotecas_id"];
            $data["data_emprestimo"] = "";
            $data["data_devolucao"] = "";


            echo "$i <br/>";

            if($this->insert($data) == "erro"){

                return "Livro: ".$name["nome"]." ID: ".$data["exemplares_id"]." Biblioteca_id: ".$data["bibliotecas_id"];
            }

        }

        return "teste";

        mysqli_close($conexao);
    }
    public function getBy($data)
    {
        // TODO: Implement getBy() method.
    }

}