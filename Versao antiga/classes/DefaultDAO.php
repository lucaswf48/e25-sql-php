<?php

/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 13/09/16
 * Time: 21:53
 */
interface DefaultDAO
{
    public function  conecta();

    public function insert($object);

    public function delete($object);

    public function deleteAll();

    public function update();

    public function getById($id);

    public function getBy($data);
}