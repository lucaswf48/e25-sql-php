<?php

/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 13/09/16
 * Time: 21:48
 */
class Livro
{
    private $id;
    private $nome;
    private $isbn;
    private $autor;
    private $tema;
    private $editora;

    function __construct($data){

        $this->nome = $data["nome"];
        $this->isbn = $data["isbn"];
        $this->autor = $data["autor"];
        $this->tema = $data["tema"];
        $this->editora = $data["editora"];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }



    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getIsbn()
    {
        return $this->isbn;
    }

    /**
     * @param mixed $isbn
     */
    public function setIsbn($isbn)
    {
        $this->isbn = $isbn;
    }

    /**
     * @return mixed
     */
    public function getAutor()
    {
        return $this->autor;
    }

    /**
     * @param mixed $autor
     */
    public function setAutor($autor)
    {
        $this->autor = $autor;
    }

    /**
     * @return mixed
     */
    public function getTema()
    {
        return $this->tema;
    }

    /**
     * @param mixed $tema
     */
    public function setTema($tema)
    {
        $this->tema = $tema;
    }

    /**
     * @return mixed
     */
    public function getEditora()
    {
        return $this->editora;
    }

    /**
     * @param mixed $editora
     */
    public function setEditora($editora)
    {
        $this->editora = $editora;
    }


}