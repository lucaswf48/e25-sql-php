<?php

/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 14/09/16
 * Time: 11:12
 */
class ExemplarDAO implements DefaultDAO
{

    public function getNumberExemplaresPorLivor(){

        $conexao = $this->conecta();

        $dado = "SELECT livros.nome,count(exemplares.id) AS exemplares FROM exemplares JOIN livros ON livros.id = exemplares.livros_id GROUP BY livros_id ORDER BY count(exemplares.id);";

        $resultado = mysqli_query($conexao,$dado);
        $linhas = mysqli_num_rows($resultado);

        for($i = 0; $i < $linhas; $i++){

            $emprestimo = mysqli_fetch_array($resultado);
            //var_dump($emprestimo);
            foreach($emprestimo as $key => $value){
               echo "$key: $value<br/>";
            }
        }
    }

    public function conecta(){

        $daw = new ConnectionFactory();
        return $daw->getConnection();
    }

    public function insert($object)
    {
        $conexao = $this->conecta();
        $exemplar = new Exemplar($object);

        $dados = "(default,\"".$exemplar->getLivroId()."\",\"".$exemplar->getBibliotecaId()."\",\"".$exemplar->getEdicao()."\",\"".$exemplar->getAnoPublicacao()."\",\"".$exemplar->getNumeroPaginas()."\");";

        if(!mysqli_query($conexao,"INSERT INTO exemplares VALUES".$dados)){
            echo("Error description: " . mysqli_error($conexao));
        }

        mysqli_close($conexao);

        return "INSERT INTO exemplares VALUES".$dados;
    }

    public function delete($object)
    {
        // TODO: Implement delete() method.
    }

    public function deleteAll()
    {
        // TODO: Implement deleteAll() method.
    }

    public function update()
    {
        // TODO: Implement update() method.
    }

    public function getById($id)
    {
        // TODO: Implement getById() method.
    }

    public function getBy($data)
    {
        // TODO: Implement getBy() method.
    }

}