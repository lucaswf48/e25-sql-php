<?php

/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 14/09/16
 * Time: 12:05
 */
class BibliotecaDAO implements DefaultDAO
{
    public function conecta()
    {
        $daw = new ConnectionFactory();

        return $daw->getConnection();
    }

    public function insert($object)
    {
        $conexao = $this->conecta();
        $biblioteca = new Biblioteca($object);

        $dados = "(default,\"".$biblioteca->getNome()."\",\"".$biblioteca->getEndereco()."\");";

        if(!mysqli_query($conexao,"INSERT INTO bibliotecas VALUES".$dados)){
            echo("Error description: " . mysqli_error($conexao));
        }

        mysqli_close($conexao);

        return "INSERT INTO bibliotecas VALUES".$dados;
    }

    public function delete($object)
    {
        // TODO: Implement delete() method.
    }

    public function deleteAll()
    {
        // TODO: Implement deleteAll() method.
    }

    public function update()
    {
        // TODO: Implement update() method.
    }

    public function getById($id)
    {
        // TODO: Implement getById() method.
    }

    public function getBy($data)
    {
        // TODO: Implement getBy() method.
    }

}