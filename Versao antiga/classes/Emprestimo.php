<?php

/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 14/09/16
 * Time: 12:55
 */
class Emprestimo
{

    private $exemplaresid;
    private $usuariosId;
    private $bibliotecasId;
    private $dataEmprestimo;
    private $dataDevolucao;

    public function __construct($data){

        $this->bibliotecasId = $data["bibliotecas_id"];
        $this->dataDevolucao = $data["data_devolucao"];
        $this->dataEmprestimo = $data["data_emprestimo"];
        $this->usuariosId = $data["usuarios_id"];
        $this->exemplaresid = $data["exemplares_id"];
    }

    /**
     * @return mixed
     */
    public function getExemplaresid()
    {
        return $this->exemplaresid;
    }

    /**
     * @param mixed $exemplaresid
     */
    public function setExemplaresid($exemplaresid)
    {
        $this->exemplaresid = $exemplaresid;
    }

    /**
     * @return mixed
     */
    public function getUsuariosId()
    {
        return $this->usuariosId;
    }

    /**
     * @param mixed $usuariosId
     */
    public function setUsuariosId($usuariosId)
    {
        $this->usuariosId = $usuariosId;
    }

    /**
     * @return mixed
     */
    public function getBibliotecasId()
    {
        return $this->bibliotecasId;
    }

    /**
     * @param mixed $bibliotecasId
     */
    public function setBibliotecasId($bibliotecasId)
    {
        $this->bibliotecasId = $bibliotecasId;
    }

    /**
     * @return mixed
     */
    public function getDataEmprestimo()
    {
        return $this->dataEmprestimo;
    }

    /**
     * @param mixed $dataEmprestimo
     */
    public function setDataEmprestimo($dataEmprestimo)
    {
        $this->dataEmprestimo = $dataEmprestimo;
    }

    /**
     * @return mixed
     */
    public function getDataDevolucao()
    {
        return $this->dataDevolucao;
    }

    /**
     * @param mixed $dataDevolucao
     */
    public function setDataDevolucao($dataDevolucao)
    {
        $this->dataDevolucao = $dataDevolucao;
    }



}