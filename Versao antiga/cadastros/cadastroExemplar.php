<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 14/09/16
 * Time: 11:23
 */

    spl_autoload_register(function ($classname) {
        require ("../classes/" . $classname . ".php");
    });

    function getInformacoes(){


        return $_POST["livros_id"]== "" || $_POST["biblioteca_id"] == "" || $_POST["edicao"] == "" || $_POST["ano_publicacao"] == "" || $_POST["num_paginas"] == "";
    }

    function adicionaExemplar(){

        $exemplardao = new ExemplarDAO();

        if(getInformacoes()){
            echo "Algum campo esta vazio";
        }
        else{
            echo $exemplardao->insert($_POST);
        }
    }

    adicionaExemplar();