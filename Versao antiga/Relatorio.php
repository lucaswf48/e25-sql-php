<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 15/09/16
 * Time: 14:04
 */

    spl_autoload_register(function ($classname) {
        require ("./classes/" . $classname . ".php");
    });


    $emprestimodao = new EmprestimoDAO();
    $exemplardao = new ExemplarDAO();

    echo $emprestimodao->getNumberEmprestimosPorBiblioteca()."<br/><br/>";
    echo $exemplardao->getNumberExemplaresPorLivor()."<br/><br/>";
    echo $emprestimodao->getNumberEmprestimosPorUsuario()."<br/><br/>";
