<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

spl_autoload_register(function ($classname) {
    require ("../classes/" . $classname . ".php");
});

$app = new \Slim\App;

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$app->get('/hello/{name}', function (Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $response->getBody()->write("Hello, $name");

    return $response;
});


$app->post('/insertBiblioteca',function(Request $request,Response $response){

    $data = $request->getParsedBody();

    $bibliotecaDAO = BibliotecaDAO::getInstance();
    $biblioteca = new Biblioteca($data);

    $bibliotecaDAO->insert($biblioteca);

});

$app->get('/getAllBibliotecas',function(Request $request, Response $response){

    $bibliotecaDAO = BibliotecaDAO::getInstance();
    $data = $bibliotecaDAO->getAll();

    return $response->withJson($data);
});

$app->run();
