<?php

/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 14/11/16
 * Time: 22:09
 */
class BibliotecaDAO implements DefaultDAO
{

    public static function getInstance() {
        static $instance = null;
        if (null === $instance) {
            $instance = new static();
        }

        return $instance;
    }

    public function conecta()
    {
        $connection = new mysqli("localhost", "root", "48h12rp6", "SistemaBibliotecario");

        if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        }

        return $connection;
    }

    public function insert($object)
    {
        $connection = $this->conecta();

        $stmt = $connection->prepare("INSERT INTO bibliotecas (nome,endereco) VALUES(?,?)");
        $stmt->bind_param("ss", $object->getNome(),$object->getEndereco());
        $stmt->execute();

    }

    public function delete($object)
    {
        // TODO: Implement delete() method.
    }

    public function deleteAll()
    {
        // TODO: Implement deleteAll() method.
    }

    public function update()
    {
        // TODO: Implement update() method.
    }

    public function getAll()
    {
        $connection = $this->conecta();
        $query = "SELECT * FROM bibliotecas";
        $result = $connection->query($query);

        while ($row = $result->fetch_assoc()){

            $data[] = $row;

        }

        return $data;
    }


    public function getById($id)
    {
        // TODO: Implement getById() method.
    }


    public function getBy($data)
    {
        // TODO: Implement getBy() method.
    }

}