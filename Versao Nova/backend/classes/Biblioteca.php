<?php
class Biblioteca{

  private $id;
  private $nome;
  private $endereco;
  private $daw;



  public function __construct($data){
    $this->id = $data["id"];
    $this->nome = $data["nome"];
    $this->endereco = $data["endereco"];
  }

  public function setId($id){
    $this->id = $id;
  }


    public function getId()
    {
        return $this->id;
    }


    public function getNome()
    {
        return $this->nome;
    }


    public function setNome($nome)
    {
        $this->nome = $nome;
    }


    public function getEndereco()
    {
        return $this->endereco;
    }


    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    
}

 ?>
